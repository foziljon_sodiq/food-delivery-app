import React from 'react'
import Helmet from "../components/Helmet/Helmet"
import CommonSection from '../components/UI/common-section/CommonSection'
import { Col, Container, Row } from 'reactstrap'
import products from "../assets/fake-data/products"
import ProductCard from '../components/UI/product-card/ProductCard'
import "../styles/all-foods.css"
import "../styles/pagination.css"
import { useEffect, useState } from 'react'
import ReactPaginate from 'react-paginate'

// 

const AllFoods = () => {

  const [searchTerm, setSearchTerm] = useState('') 
  const searchedProduct = products.filter((item)=>{
    if(searchTerm == "")return item  
    if(item.title.toLowerCase().includes(searchTerm.toLowerCase())) return item
  })

  const [pageNumber, setPageNumber] = useState(0)
  const productPerPage = 8
  const visitedPage = pageNumber * productPerPage
  const displayPage = searchedProduct.slice(visitedPage, visitedPage + productPerPage)
  const pageCount = Math.ceil(searchedProduct.length / productPerPage)
  const changePage = ({selected})=>{
    setPageNumber(selected)
  }
  return ( 
    <Helmet title={"All-Foods"}>
      <CommonSection title={" All Foods"}>
      </CommonSection>
        <section>
          <Container>
            <Row> 
              <Col lg="6" md="6" sm="6">
                <div className="search__widget d-flex align-items-center justify-content-between  w-50">
                  <input type="text" placeholder=" I'm looking" 
                  value={searchTerm} onChange={(e) => setSearchTerm(e.target.value)} />
                  <span className='text-right'><i className='ri-search-line'></i></span>
                </div>
              </Col>
              <Col lg="6" md="6" sm="6" className='mb-5'>
                <div className="sorting__widget text-end ">
                  <select name="" id="" className='w-50'>
                    <option value="">Default</option>
                    <option value="ascending">Alphabetically, A-Z</option>
                    <option value="descending">Alphabetically, Z-A</option>
                    <option value="high-price">High price</option>
                    <option value="low-price">Low price</option>
                  </select>
                </div>
              </Col>
              {
                displayPage?.map((item, index)=>(
                <Col lg={3} md={4} sm={6} xs={6} className="mb-4">
                  <ProductCard item={item} key={index}/>
                </Col>))
              }

              <div className="">
                <ReactPaginate
                  pageCount={pageCount}
                  onPageChange={changePage}
                  prevPageRel
                  previousLabel='Prev'
                  nextLabel="Next"
                  containerClassName='paginationButtons'
                />
              </div>
            </Row>
          </Container>
        </section>
      
    </Helmet>
  )
}

export default AllFoods