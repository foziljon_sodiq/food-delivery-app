import React, { useEffect, useState } from 'react'
import { Col, Container, ListGroup, ListGroupItem, Row } from 'reactstrap'
import Helmet from "../components/Helmet/Helmet.js"
import heroImg from "../assets/images/hero.png"
import "../styles/hero-section.css"
import { Link } from 'react-router-dom'

import Category from '../components/UI/category/Category.jsx'
import "../styles/home.css"
import featureImg1  from "../assets/images/service-01.png"
import featureImg2  from "../assets/images/service-02.png"
import featureImg3  from "../assets/images/service-03.png"
// 
import products  from "../assets/fake-data/products.js"
//
import foodCategoryImg1  from "../assets/images/hamburger.png"
import foodCategoryImg2  from "../assets/images/pizza.png"
import foodCategoryImg3  from "../assets/images/bread.png"
import whyImg from "../assets/images/location.png"
//
import networking from "../assets/images/network.png"
import ProductCard from '../components/UI/product-card/ProductCard.jsx'

import TestimonialSlide from '../components/UI/slider/TestimonialSlide.jsx'

const featureData = [
  {
    title: " Quick delivery",
    imgUrl: featureImg1 ,
    desc: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni quia fugit tempora! Unde, voluptas! Dicta"
  }, 
  {
    title: " Super Dine In ",
    imgUrl: featureImg2 ,
    desc: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni quia fugit tempora! Unde, voluptas! Dicta"
  }, 
  {
    title: "Easy Pick Up",
    imgUrl: featureImg3 ,
    desc: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Magni quia fugit tempora! Unde, voluptas! Dicta"
  }, 
]
// 
const Home = () => {

  const [category, setCategory] = useState("ALL")
  const [allProducts, setAllProducts] = useState(products)
  const [hotPizza, setHotPizza] = useState([])

  useEffect(()=>{
    const filterProducts = products.filter((item)=> item.category === "Pizza" )
    const slicePizza = filterProducts.slice(0,4)
    setHotPizza(slicePizza)
  },[])
  //
  useEffect(()=>{
    if(category === "ALL"){
      setAllProducts(products)
    }
    if(category === "BURGER"){
      const filterProducts = products.filter((item)=> item.category === "Burger" )
      setAllProducts(filterProducts)
    }
    if(category === "PIZZA"){
      const filterProducts = products.filter((item)=> item.category === "Pizza" )
      setAllProducts(filterProducts)
    }
    if(category === "BREAD"){
      const filterProducts = products.filter((item)=> item.category === "Bread" )
      setAllProducts(filterProducts)
    }
  },[category])
  return (
    <Helmet title="Home">
      <section>
        <Container>
          <Row>
            <Col lg="6" md="6">
              <div className="hero__content">
                <h5 className='mb-3'>Easy way to make an order</h5>
                <h1 className='mb-4 hero__title'><span>HUNGRY ?</span> Just wait <br />  food at <span> your door </span></h1>
                <p>Lorem ipsum dolor, sit amet consectetur adipisicing elit. Dolores nobis est porro fuga nihil quis.</p>
                <div className="hero__btns d-flex align-items-center gap-5 mt-4">
                  <button className='order__btn d-flex align-items-center justify-content-between'>
                    Order now <i className='ri-arrow-right-s-line'></i>
                  </button>
                  <button className='foods__btn'><Link to="/foods">See all foods</Link></button>
                </div>

                <div className="hero__service d-flex align-items-center gap-5 mt-5">
                  <p className='d-flex align-items-center gap-2'>
                    <span className='shopping__icon'>
                      <i className='ri-car-line'></i>
                    </span>
                    No shopping charge
                  </p>
                  <p className='d-flex align-items-center gap-2'>
                    <span className='shopping__icon'>
                      <i className='ri-shield-check-line'></i>
                      </span>
                    100% secure checkout
                  </p>
                </div>
              </div>
            </Col>
            <Col lg="6" md="6">
              <div className="hero__img">
                <img src={heroImg} alt="hero-image" className='w-100'/>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
      <section className='pt-0'>
        <Category/>
      </section>
      <section className=''>
        <Container>
          <Row>
            <Col lg="12" className='text-center'>
                <h5 className='feature__subtitle mb-4'>What we serve</h5>
                <h2 className='feature__title'>Just sit back at home </h2>
                <h2 className='feature__title'>we will <span>take care </span></h2>
                <p className='mb-1 mt-4 feature__text'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, dignissimos.</p>
                <p className='feature__text'>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Non, dignissimos.</p>
            </Col>
            {
              featureData.map((item, index)=>(
                <Col lg="4" md="4" sm="6" key={index} className="mt-5">
                  <div className="feature__item text-center px-5 py-3">
                    <img src={item.imgUrl} className="feature__item-img w-25 mb-3 text-center" alt="feature__image" />
                    <h5 className='fw-bold mb-3'>{item.title}</h5>
                    <p>{item.desc}</p>
                  </div>
                </Col>
              ))
            }
          </Row>
        </Container>
      </section>
      <section>
        <Container>
          <Row>
            <Col lg="12">
              <h2 className='text-center'>Popular Foods</h2>
            </Col>
            <Col lg="12">
              <div className="food__category d-flex align-items-center justify-content-center gap-4">
                <button className={`all__btn  ${category === "ALL" && "foodBtnActive"}`} onClick={()=>setCategory("ALL")}>All...</button>
                <button className={`d-flex align-items-center gap-2 ${category === "BURGER" && "foodBtnActive"}`} onClick={()=>setCategory("BURGER")}><img src={foodCategoryImg1} alt="food-category-image"/>Burger</button>
                <button className={`d-flex align-items-center gap-2 ${category === "PIZZA" && "foodBtnActive"}`} onClick={()=>setCategory("PIZZA")}><img src={foodCategoryImg2} alt="food-category-image"/>Pizza</button>
                <button className={`d-flex align-items-center gap-2 ${category === "BREAD" && "foodBtnActive"}`} onClick={()=>setCategory("BREAD")}><img src={foodCategoryImg3} alt="food-category-image"/>Bread</button>
              </div>  
            </Col>

            {
              allProducts.map((item, index)=>(
                <Col lg="3" md="4" key={index} className="mt-5">
                  <ProductCard item={item}/>
                </Col>
              ))
            } 
          </Row>
        </Container>
      </section>
      <section className='why__choose-us'>
        <Container>
          <Row>
            <Col lg="6" md="6">
              <img src={whyImg} className="w-100" alt="why-this-fast-food" />
            </Col>
            <Col lg="6" md="6">
                <div className="why__fastFood  mb-3">
                  <h2 className="fastFood__title">Why  <span>tasty treat</span></h2>
                  <p className='fastFood__desc'>
                     Lorem ipsum dolor sit amet consectetur adipisicing
                     elit. Repellendus pariatur placeat, praesentium fugiat 
                     vero blanditiis nostrum. Numquam harum soluta, cum, expedita
                     doloremque nulla facere possimus 
                     delectus exercitationem vel, reiciendis sapiente.
                  </p>
                  <ListGroup className='mt-5'>
                    <ListGroupItem className='border-0 ps-0'>
                      <p className='choose__us-title d-flex align-items-center gap-2'>
                        <i className='ri-checkbox-circle-line'></i>
                        Fresh and tasty foods
                      </p>
                      <p className='choose__us-desc'>
                          Lorem ipsum dolor sit, amet consectetur 
                          adipisicing elit. Sapiente quae accusamus
                      </p>
                    </ListGroupItem>
                    <ListGroupItem className='border-0 ps-0'>
                      <p className='choose__us-title  d-flex align-items-center gap-2'>
                        <i className='ri-checkbox-circle-line'> </i>
                        Quality support
                      </p>
                      <p className='choose__us-desc'>
                          Lorem ipsum dolor sit, amet consectetur 
                          adipisicing elit. Sapiente quae accusamus
                      </p>
                    </ListGroupItem>
                    <ListGroupItem className='border-0 ps-0'>
                      <p className='choose__us-title d-flex align-items-center gap-2' >
                        <i className='ri-checkbox-circle-line'> </i>
                        Order from any location
                      </p>
                      <p className='choose__us-desc'>
                          Lorem ipsum dolor sit, amet consectetur 
                          adipisicing elit. Sapiente quae accusamus
                      </p>
                    </ListGroupItem>
                  </ListGroup>
                </div>

            </Col>
          </Row>
        </Container> 
      </section>
      <section className='pt-0'>
        <Container>
          <Row>
            <Col lg="12" className='text-center '>
              <h2 className=''>Hot Pizza </h2>
            </Col> 
              {
                hotPizza.map((item, index)=>{
                  return <Col lg="3" md="4">
                    <ProductCard item={item}></ProductCard>
                     </Col>
                })
              } 
          </Row>
        </Container>
      </section>
      <section>
        <Container>
          <Row>
            <Col lg="6" md="6">
              <div className="testimonial">
                <h5 className='testimonial__subtitle mb-4'>Testimonial</h5>
                <h2 className='testimonial__title mb-4'> What our <span>customers</span>  are saying</h2>
                <p className="testimonial__desc">
                  Lorem ipsum dolor sit amet consectetur adipisicing elit. Harum 
                  accusantium corporis aut voluptatibus quibusdam unde?
                </p>
                <TestimonialSlide/>
              </div>
            </Col>
            <Col lg="6" md="6">
              <img src={networking} className="w-100" alt="networking img" />
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  )
}

export default Home