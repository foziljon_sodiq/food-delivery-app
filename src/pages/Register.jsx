import React, {useRef} from 'react'
import { Link } from 'react-router-dom'
import { Container, Row, Col } from 'reactstrap'
import Helmet from '../components/Helmet/Helmet'
import CommonSection from '../components/UI/common-section/CommonSection'

const Register = () => {
  const registerNameRef = useRef()
  const registerPasswordRef = useRef()
  const registerEmailRef = useRef()

  const submitHandler = e => {
    e.preventDefault()

  }

  return (
    <Helmet title="Register">
      <CommonSection title="Register"></CommonSection>
      <section>
        <Container>
          <Row>
            <Col lg="6" m="6" sm="12" className='m-auto text-center' >
              <form  className='form mb-5' onSubmit={submitHandler}>
                <div className="form__group">
                  <input type="text" placeholder='Full name' required  ref={registerNameRef}/>
                </div>
                <div className="form__group">
                  <input type="email" placeholder='Email' required  ref={registerEmailRef}/>
                </div>
                <div className="form__group">
                  <input type="password" placeholder='Password' required  ref={registerPasswordRef}/>
                </div>
                <button type='submit' className="addToCart__btn">Sign Up</button>
              </form>
              <Link to="/login">Don't have an account? Login</Link>
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  )
}

export default Register