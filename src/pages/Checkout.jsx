import React, {useState} from 'react'
import { useSelector } from 'react-redux'
import { Container, Row,Col } from 'reactstrap'
import Helmet from '../components/Helmet/Helmet'
import CommonSection from '../components/UI/common-section/CommonSection'
import "../styles/checkout.css"

const Checkout = () => {
  const [enterName, setEnterName] = useState("") 
  const [enterEmail, setEmail] = useState("") 
  const [enterNumber, setNumber] = useState("") 
  const [enterCountry, setCountry] = useState("") 
  const [enterCity, setCity] = useState("") 
  const [postalCode, setPostalCode] = useState("")  

  const shippingInfo = []
  const cartTotalAmount = useSelector(state=> state.cart.totalAmount)
  const shippingCost = 30

  const totalAmount = cartTotalAmount + Number(shippingCost)

  const submitHandler = e => {
    e.preventDefault()
    const userShippingAddress = {
      name: enterName,
      email: enterEmail,
      phone: enterNumber,
      country: enterCountry,
      city: enterCity,
      postalCode: postalCode
    }
    shippingInfo.push(userShippingAddress)
    console.log(shippingInfo);
  }
  return (
    <Helmet title="Checkout">
      <CommonSection title="Checkout">      
      </CommonSection>
       <section>
          <Container>
            <Row>
              <Col lg="8" md="6">
                <h6 className='mb-4'>Shipping Address</h6>
                <form action="" className='checkout__form' onSubmit={submitHandler}>
                  <div className="form__group">
                    <input type="text"  placeholder='Enter your name' 
                      required onChange={e => setEnterName(e.target.value)}/>
                  </div>
                  <div className="form__group">
                    <input type="email"  placeholder='Enter your email'
                    required onChange={e => setEmail(e.target.value)}/>
                  </div>
                  <div className="form__group">
                    <input type="number"  placeholder='Phone number'
                    required onChange={e => setNumber(e.target.value)}/>
                  </div>
                  <div className="form__group">
                    <input type="text"  placeholder='Country'
                    required onChange={e => setCountry(e.target.value)}/>
                  </div>
                  <div className="form__group">
                    <input type="text"  placeholder='City'
                    required onChange={e => setCity(e.target.value)}/>
                  </div>
                  <div className="form__group">
                    <input type="text"  placeholder='Postal code '
                    required onChange={e => setPostalCode(e.target.value)}/>
                  </div>
                    <button className='addToCart__btn'>Payment</button>
                </form>
          
              </Col>
              <Col lg="4" md="6">
                <div className='checkout__bill'>
                  <h6 className='d-flex align-items-center justify-content-between mb-3'>Subtotal: <span>{cartTotalAmount}$</span></h6>
                  <h6 className='d-flex align-items-center justify-content-between mb-3'>Shipping: <span>{shippingCost}$</span></h6>
                  <div className='checkout__total'>
                    <h5 className='d-flex align-items-center justify-content-between'>
                      Total: <span>${totalAmount}</span>
                    </h5>
                  </div>
                </div>
              </Col>
            </Row>
          </Container>
        </section>

    </Helmet>
  )
}

export default Checkout