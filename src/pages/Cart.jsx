import React from 'react'
import Helmet from '../components/Helmet/Helmet'
import CommonSection from '../components/UI/common-section/CommonSection'

import { useSelector, useDispatch } from 'react-redux'
import { Container, Row,Col } from 'reactstrap'
import "../styles/cart-page.css"

import { cartActions } from '../store/shopping-cart/CartSlice'
import { Link } from 'react-router-dom'
const Cart = () => {
  const cartItems = useSelector(state => state.cart.cartItems)
  const totalAmount = useSelector(state => state.cart.totalAmount)
  console.log(totalAmount);


  return (
    <Helmet title="Cart">
      <CommonSection title="Your Cart"/>
      <section>
        <Container>
          <Row>
            <Col lg="12">
              {
                cartItems.length === 0 ? <h2 className='text-center'>Your Cart is empty</h2> :
                <table className='table table-bordered valign-center'>
                <thead className='border-1'>
                  <tr className='text-center'>
                    <th>Image</th>
                    <th>Product title</th>
                    <th>Price </th>
                    <th>Quantity</th>
                    <th>Delete</th>
                  </tr>
                </thead>
                <tbody>
                  {
                    cartItems.map( item => <Tr item={item}/> )
                  }
                </tbody>
              </table>
              }

              <div className="subtotal mt-5">
                <h6 className='cart__subtotal'>Subtotal: <span>{totalAmount}$</span></h6>
                <p>Taxes and shipping will calculate at checkout</p>
                <div className='cart__page-btn'>
                  <button className='addToCart__btn me-4'><Link to="/foods"> Continue shopping </Link></button>
                  <button className='addToCart__btn'><Link to="/checkout"> Proceed checkout</Link></button>
                </div>
              </div>
            </Col>
          </Row>
        </Container>
      </section>
    </Helmet>
  )
}

const Tr = props => {

  const {id,image01, price, title, quantity} = props.item
  const dispatch = useDispatch()

  const deleteItem = () =>{
    dispatch(cartActions.deleteItem(id))
  }

  return <tr className='text-center'>
    <td className='cart__image-box'><img src={image01} alt="cart image" /></td>
    <td>{title}</td>
    <td>{price}$</td>
    <td>{quantity}px</td>
    <td className='cart__item-del'><i className='ri-delete-bin-line' onClick={deleteItem}></i></td>
  </tr>
}

export default Cart