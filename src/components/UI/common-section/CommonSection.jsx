import React from 'react'
import { Col, Container, Row } from 'reactstrap'
import "../../../styles/common-section.css"
const CommonSection = (props) => {
  return (
    <section className='common__section'>
        <Container>
            <h2 className='text-white'>{props.title}</h2>
        </Container>
    </section>
  )
}

export default CommonSection