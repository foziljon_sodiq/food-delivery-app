import React from 'react'
import { useDispatch } from 'react-redux'
import { ListGroupItem } from 'reactstrap'
import productImg from "../../../assets/images/product_01.1.jpg"
import { cartActions } from '../../../store/shopping-cart/CartSlice'

import "../../../styles/cart-item.css"
//
const CartItem = ({item}) => {
  const {id, price, title, image01, quantity, totalPrice} = item
  const dispatch = useDispatch()

  const incrementItem = () => {
    dispatch(cartActions.addItem({
        id,
        title,
        price,
        image01
    }))
  }

  const decreaseItem =()=>{
    dispatch(cartActions.removeItem(id))
  }

  const deleteItem =()=>{
    dispatch(cartActions.deleteItem(id))
  }
   
  return (
    <> 
        <ListGroupItem className='border-0 cart__item'>
            <div className="cart__item-info d-flex gap-2">
                <img src={image01} alt="product image" />
                <div className="cart__product-info d-flex align-items-center justify-content-between gap-4 w-100">
                    <div className="">
                        <h6 className='cart__product-title'>{title}</h6>
                        <p className='cart__product-price d-flex align-items-center gap-5'>{quantity}x <span>${totalPrice}</span></p>
                        <div className="increase__decrease-btn d-flex align-items-center justify-content-between">
                            <span className='increase__btn' onClick={incrementItem}><i className='ri-add-line'></i></span>
                            <span className='quantity'>{quantity}</span>
                            <span className='decrease__btn' onClick={decreaseItem}><i className='ri-subtract-line'></i></span>
                        </div>
                    </div>
                    <span className='delete__line' onClick={deleteItem}><i className='ri-close-line'></i></span>
                </div>
            </div>
        </ListGroupItem>
    </>
  )
}

export default CartItem