import React from 'react'
import Slider from "react-slick";
//
import ava1 from "../../../assets/images/ava-1.jpg"
import ava2 from "../../../assets/images/ava-2.jpg"
import ava3 from "../../../assets/images/ava-3.jpg"
import ava4 from "../../../assets/images/ava-4.jpg"

import "../../../styles/slider.css"
//
const TestimonialSlide = () => {
    const settings = {
      dots: true,
      infinite: true,
      autoplay: true,
      speed: 700,
      autoplaySpeed: 2500,
      swipeToSlide: true,
      slidesToShow: 1,
      slidesToScroll: 1
    }
  return (
    <>
    
        <Slider {...settings}>
          <div>
            <p className="review__text">Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Voluptatum aut optio, animi vitae odit illo quis consectetur?
                Provident beatae sit ut corrupti commodi aliquam 
                rerum adipisci ea? Quod, magnam aperiam!
            </p>
            <div className="slider__content d-flex align-items-center gap-3">
                <img src={ava1} className="rounded" alt="avatar" />
                <h6> Joaquin Phoenix </h6>
            </div>
          </div>
          <div>
            <p className="review__text">Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Voluptatum aut optio, animi vitae odit illo quis consectetur?
                Provident beatae sit ut corrupti commodi aliquam 
                rerum adipisci ea? Quod, magnam aperiam!
            </p>
            <div className="slider__content d-flex align-items-center gap-3">
                <img src={ava2} className="rounded"  alt="avatar" />
                <h6> Jon Thompson </h6>
            </div>
          </div>
          <div>
            <p className="review__text">Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Voluptatum aut optio, animi vitae odit illo quis consectetur?
                Provident beatae sit ut corrupti commodi aliquam 
                rerum adipisci ea? Quod, magnam aperiam!
            </p>
            <div className="slider__content d-flex align-items-center gap-3">
                <img src={ava3} className="rounded"  alt="avatar" />
                <h6> Ben Arnold </h6>
            </div>
          </div>
          <div>
            <p className="review__text">Lorem ipsum dolor sit amet consectetur, 
                adipisicing elit. Voluptatum aut optio, animi vitae odit illo quis consectetur?
                Provident beatae sit ut corrupti commodi aliquam 
                rerum adipisci ea? Quod, magnam aperiam!
            </p>
            <div className="slider__content d-flex align-items-center gap-3">
                <img src={ava4} className="rounded"  alt="avatar" />
                <h6> Adrian Smit</h6>
            </div>
          </div>
         
        </Slider>
    </>
  )
}

export default TestimonialSlide