import React, { useRef, useEffect } from 'react'
import { NavLink, Link } from 'react-router-dom'
import { Container } from 'reactstrap'
import logo from "../../assets/images/res-logo.png"
import { useSelector, useDispatch } from 'react-redux'
import { cartUiActions } from '../../store/shopping-cart/CartUiSlice'
// styles
import  "../../styles/header.css"

const nav_links = [
    {
      display: "Home",
      path: "/"
    },
    {
      display: "Foods",
      path: "/foods"
    },
    {
      display: "Cart",
      path: "/cart"
    },
    {
      display: "Contact",
      path: "/contact"
    },
  ]
// 
function Header() {
  const menuRef = useRef(null)
  const headerRef = useRef(null)
  const totalQuantity = useSelector(state => state.cart.totalQuantity ) 
  const dispatch = useDispatch()
  //
  const toggleMenu = () => {
    menuRef.current.classList.toggle("show__menu")
  } 

  const toggleCart = () =>{
    dispatch(cartUiActions.toggle())
  }

  useEffect(()=>{
    window.addEventListener("scroll", ()=>{
        if(document.body.scrollTop > 80 || document.documentElement.scrollTop > 80 ){
          headerRef.current.classList.add('header__shrink')
        }
        else{
          headerRef.current.classList.remove('header__shrink')
        }
        // window.removeEventListener('scroll')
    })
  },[])

  return (
    <div className='header ' ref={headerRef}>
      <Container>
        <div className="nav__wrapper d-flex align-items-center justify-content-between">
          <div className="logo">
            <Link to="/">
              <img src={logo} alt="logo"  />
              <h5>Tasty Treat</h5>
            </Link> 
          </div>

          {/* ====== menu ====== */} 
          <div className="navigation" ref={menuRef}>
            <div className="menu d-flex align-items-center gap-5">
              {
                nav_links.map((item,index)=>{
                  return <NavLink to={item.path} key={index} onClick={toggleMenu} className={(navClass)=> navClass.isActive && "active__menu"}>{item.display}</NavLink>
                })
              }
            </div>
          </div>

          {/* ====== nav right icons  ====== */}
          <div className="nav__right d-flex align-items-center gap-4">
              <span className="cart__icon" onClick={toggleCart}>
                <i className='ri-shopping-basket-line'></i>
                <span className="cart__badge">{totalQuantity}</span>
              </span>

              <span className='user'>
                <Link to="/login"><i className='ri-user-line'></i></Link>
              </span>

              <span className="mobile__menu" onClick={toggleMenu}> <i className="ri-menu-line"></i></span>
          </div>
 
        </div>
   
      </Container>
    </div>
  )
}

export default Header