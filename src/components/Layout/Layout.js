import React from "react";
import Cart from "../../pages/Cart";
import Routers from "../../routes/Routers";
import Footer from "../Footer/Footer";
import Header from "../Header/Header";
import Carts from "../UI/cart/Carts";
import { useSelector } from "react-redux";
function Layout() {
  const showCart = useSelector((state) => state.cartUi.cartVisible);
  return (
    <div>
      <Header />
      {showCart && <Carts />}

      <div className="">
        <Routers />
      </div>
      <Footer />
    </div>
  );
}

export default Layout;
